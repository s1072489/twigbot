import nextcord
from nextcord.ext import commands
import platform


class General(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.pythonVersion = platform.python_version()
        self.nextcordVersion = nextcord.__version__

    @commands.command()
    async def info(self, ctx):
        serverCount = len(self.client.guilds)
        memberCount = len(set(self.client.get_all_members()))

        embed = nextcord.Embed(
            title=f"{self.client.user.name} Stats",
            description="\uFEFF",
            colour=ctx.author.colour,
            timestamp=ctx.message.created_at,
        )

        embed.add_field(name="Bot Version:", value="0.2.0.0")
        embed.add_field(name="Python Version:", value=self.pythonVersion)
        embed.add_field(name="Nextcord Version", value=self.nextcordVersion)
        embed.add_field(name="Total Guilds:", value=serverCount)
        embed.add_field(name="Total Users:", value=memberCount)
        embed.add_field(name="Bot Developers:",
                        value="<@740637592713691217> | <@760325335777804340> | <@751405982562648146> | <@697323031919591454> | <@744715959817994371> | <@718881269714518017>")
        embed.set_footer(text=f"[Support Server](https://nextcord.gg/ZsZQ4SHsqs)")

        embed.set_footer(text=f"Carpe Noctem | {self.client.user.name}")

        await ctx.send(embed=embed)

    @commands.command(aliases=["say"])
    async def embed(self, ctx, title, *, arg):
        await ctx.message.delete()
        title = title.replace("_", " ")
        embed = nextcord.Embed(colour=nextcord.Colour.blurple())
        embed.add_field(name=f'{title}', value=f"{arg}", inline=False)
        embed.set_footer(text=f'Embed by {ctx.message.author.name}')
        await ctx.channel.send(embed=embed)

    @commands.command()
    async def serverinfo(self, ctx):
        await ctx.message.delete()
        embed = nextcord.Embed(
            title=f"{ctx.guild.name} Server Information",
            color=discord.Color.blue()
        )
        embed.set_thumbnail(url=ctx.guild.icon_url)
        embed.add_field(name="Owner", value=f"<@{ctx.guild.owner_id}>", inline=True)
        embed.add_field(name="Region", value=ctx.guild.region, inline=True)
        embed.add_field(name="Created on",
                        value=ctx.guild.created_at.strftime("%d/%m/%Y"))
        embed.add_field(name="Member Count",
                        value=ctx.guild.member_count, inline=True)
        embed.add_field(
            name="Boosts", value=ctx.guild.premium_subscription_count)
        await ctx.send(embed=embed)

    @commands.command(aliases=["vote"])
    async def poll(self, ctx, *, message):
        await ctx.message.delete()
        embed = nextcord.Embed(colour=nextcord.Colour.blue())
        embed.add_field(name='React to this message to cast your vote!', value=f"{message}", inline=False)
        embed.set_footer(text=f'A poll by {ctx.message.author.name}')
        msg = await ctx.channel.send(embed=embed)
        await msg.add_reaction("<:yes:853606249119744020>")
        await msg.add_reaction("<:no:853606249140977694>")

    @commands.command(aliases=['user_info', 'whois'])
    async def userinfo(self, ctx, member: nextcord.Member):
        embed = nextcord.Embed(
            colour=member.colour
        )
        embed.set_thumbnail(url=member.avatar_url)
        embed.set_author(name=f"User information {member}")
        embed.add_field(name="Joined at", value=member.joined_at.strftime(
            "%d/%m/%Y"), inline=True)
        embed.add_field(name="Account created at",
                        value=member.created_at.strftime("%d/%m/%Y"), inline=True)
        embed.add_field(name="Top role",
                        value=member.top_role.mention, inline=True)
        embed.add_field(name="Id", value=member.id, inline=True)
        embed.add_field(name="Nickname", value=member.nick, inline=True)
        embed.add_field(name="Bot?", value=member.bot, inline=True)
        embed.set_footer(text=f"Requested by |{ctx.message.author}|")
        await ctx.channel.send(embed=embed)
        await ctx.message.delete()


def setup(client):
    client.add_cog(General(client))
